exports.up = function(knex, Promise) {
  return knex.schema.createTable('user_categories', t => {
    t.increments().primary();
    t.integer('user_id').references('users.id');
    t.integer('category_id').references('categories.id');
    t.unique(['user_id', 'category_id']);
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user_categories'); 
};
