exports.up = function(knex, Promise) {
  return knex.schema.createTable('likes', t => {
    t.increments().primary();
    t.integer('post_id').references('posts.id');
    t.integer('user_id').references('users.id');
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('likes'); 
};
