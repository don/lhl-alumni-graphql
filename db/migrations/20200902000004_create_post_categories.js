exports.up = function(knex, Promise) {
  return knex.schema.createTable('posts_categories', t => {
    t.increments().primary();
    t.integer('post_id').references('posts.id');
    t.integer('category_id').references('categories.id');
    t.unique(['post_id', 'category_id']);
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('posts_categories'); 
};
