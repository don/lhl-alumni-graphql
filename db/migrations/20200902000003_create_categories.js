exports.up = function(knex, Promise) {
  return knex.schema.createTable('categories', t => {
    t.increments().primary();
    t.string('name');
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('categories'); 
};
