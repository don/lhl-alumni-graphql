exports.up = function(knex, Promise) {
  return knex.schema.createTable('posts', t => {
    t.increments().primary();
    t.string('title');
    t.string('content');
    t.boolean('published');
    t.integer('user_id').notNullable().references('users.id');
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('posts'); 
};
