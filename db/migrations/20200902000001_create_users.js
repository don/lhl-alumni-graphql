exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', t => {
    t.increments().primary();
    t.string('email').notNullable().unique();
    t.string('first_name');
    t.string('last_name');
    t.boolean('enabled').notNullable().defaultTo(true);
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users'); 
};
