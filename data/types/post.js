const typeDef = `
type Post {
  id: ID!
  author: User
  title: String
  categories: [Category]
  content: String
  likes: Int
}
`;

module.exports = typeDef;

