const typeDef = `
type Category {
  id: ID!
  name: String!
}
`;

module.exports = typeDef;
