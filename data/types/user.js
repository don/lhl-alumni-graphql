const typeDef = `
type User {
  id: ID!
  first_name: String
  last_name: String
  name: String
  email: String
  categories: [Category]
  posts: [Post]
  since: String
}
`;

module.exports = typeDef;
