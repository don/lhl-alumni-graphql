const moment = require('moment');

const resolvers = {
  name: (userObj) => {
    return `${userObj.first_name} ${userObj.last_name}`.trim();
  },
  since: (userObj, args, context) => {
    return moment(userObj.created_at).format();
  },
  categories: (userObj, args, context) => {
    const { knex } = context.app;

    return knex.select('c.name')
      .from('categories AS c')
      .leftJoin('user_categories AS uc', 'uc.category_id', 'c.id')
      .where('uc.user_id', userObj.id)
      .orderBy('c.name', 'asc');
  },
  posts: (userObj, args, context) => {
    const { knex } = context.app;

    return knex('posts').where('user_id', userObj.id);
  }
};

module.exports = resolvers;
