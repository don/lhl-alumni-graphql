const resolvers = {
  author: async (postObj, args, context) => {
    const { knex } = context.app;

    return (await knex('users').where('id', postObj.user_id).limit(1))[0];
  },
  categories: (postObj, args, context) => {
    const { knex } = context.app;

    return knex.select('c.id', 'c.name').from('categories AS c').leftJoin('posts_categories AS pc', 'pc.category_id', 'c.id').where('pc.post_id', postObj.id);
  },
  likes: async (postObj, args, context) => {
    const { knex } = context.app;

    const result = await knex('likes').count({count: 'id'}).where('post_id', postObj.id);

    return result[0].count;
  }
};

module.exports = resolvers;
