const { likeContent }  = require('./handleContent');

const queries = {
  users: (root, args, context) => {
    const { knex } = context.app;
    return knex('users');
  },
  user: async (root, args, context) => {
    const { knex } = context.app;
    const { id } = args;

    return (await knex('users').where('id', +id).limit(1))[0];
  },
  categories: (root, args, context) => {
    const { knex } = context.app;

    return knex('categories');
  },
  posts: (root, args, context) => {
    const { knex } = context.app;
    const { userId } = args;

    return knex('posts').where('user_id', +userId);
  },
  postsByCategory: (root, args, context) => {
    const { knex } = context.app;
    const { categoryId } = args;

    return knex('posts AS p').leftJoin('posts_categories AS pc', 'pc.post_id', 'p.id').where('pc.category_id', +categoryId);
  },
  usersByCategory: (root, args, context) => {
    const { knex } = context.app;
    const { categoryId } = args;

    return knex('users AS u').leftJoin('user_categories AS uc', 'uc.user_id', 'u.id').where('uc.category_id', +categoryId);
  }
};

const mutations = {
  newPost: async (root, args, context) => {
    const { knex } = context.app;
    const { title, content } = args;

    const newPost = {
      title: title.trim(),
      content: content.replace(/\s{2,}/, '').trim(),
      published: true,
      user_id: 1
    }

    return (await knex('posts').insert(newPost).returning('*'))[0];
  },
  categorize: async (root, args, context) => {
    const { knex } = context.app;
    const { postId, categoryId } = args;

    //Validate the categoryId
    const category = (await knex('categories').where('id', +categoryId).limit(1))[0];
    const post = (await knex('posts').where('id', +postId).limit(1))[0];

    if (category && post) {
      // Content is validated, let's continue
      const results = await knex('posts_categories').insert({post_id: post.id, category_id: category.id});

      return Boolean(results && results.length);
    } else {
      throw new Error('Mwapmwapwaaa');
    }
  },
  like: (root, args, context) => {
    return likeContent(args.postId, context.app);
  }
};

module.exports = { queries, mutations };
