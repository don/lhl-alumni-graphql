async function likeContent(id, context) {
  const { knex } = context;

  const liked = await knex('likes').insert({user_id: 1, post_id: +id}).returning('*');

  return Boolean(liked.length);
}

module.exports = {
  likeContent
};
