const { queries, mutations }  = require('./main');
const userResolvers           = require('./user');
const postResolvers           = require('./post');

module.exports = { 
  Query: queries, 
  Mutation: mutations, 
  User: userResolvers, 
  Post: postResolvers,
};

