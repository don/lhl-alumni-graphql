const { makeExecutableSchema } = require('graphql-tools');
const typeDefs = require('./types');
const resolvers = require('./resolvers');

const schema = `
  type Query {
    users: [User]
    user(id: ID!): User
    posts(userId: ID!): [Post]
    categories: [Category]
    postsByCategory(categoryId: ID!): [Post]
    usersByCategory(categoryId: ID!): [User]
  }
  type Mutation {
    newPost(title: String!, content: String!): Post 
    categorize(postId: ID!, categoryId: ID!): Boolean
    like(postId: ID!): Boolean
  }
`;

module.exports = makeExecutableSchema({ typeDefs: [ schema, ...typeDefs ].join(''), resolvers });
