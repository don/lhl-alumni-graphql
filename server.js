const fastify   = require('fastify')({ logger: true });
const helmet    = require('fastify-helmet');
const config    = require('./config');
const gql       = require('fastify-gql');

const schema    = require('./data/schema');
const resolvers = require('./data/resolvers');

fastify.register(helmet)
  .register(require('fastify-sensible'))
  .register(require('fastify-formbody'))
  .register(require('fastify-knex'), {
    client: 'pg',
    debug: (config.get('env') === 'development'),
    connection: config.get('db')
  })
  .register(gql, { graphiql: true, schema });

fastify.route({
  method: 'POST',
  url: '/gql',
  handler: async (request, reply) => {
    const query = request.body.query;
    if (process.env.NODE_ENV === 'development') {
      request.log.info({msg: 'Got a request', payload: query});
    }

    // Second param, if supplied, is graphql context
    return fastify.graphql(query);
  }
});

const start = async () => {
  try {
    await fastify.listen(config.get('port'));
    fastify.log.info(`GraphQL server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
