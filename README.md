# LHL Alumni GraphQL Demo

# Setup

```
git clone git@gitlab.com:don/lhl-alumni-graphql.git
cd lhl-alumni-graphql
npm i
npx knex migrate:latest
cp .env.template .env.development
```

Edit the file `.env.development` with any relevant values.

## Running the server

```
npm start
```

## Usage

Check out `data/schema.js` for available queries and mutations.

Built on [Fastify](https://fastify.io) by [Don Burks](https://donburks.com).



